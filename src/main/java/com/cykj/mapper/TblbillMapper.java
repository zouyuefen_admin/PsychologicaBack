package com.cykj.mapper;

import com.cykj.bean.Tblbill;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblbillMapper {
    //查找资金账户列表
    public List<Tblbill> findAccountList(@Param("managerId") long managerId,
                                         @Param("index") int index,
                                         @Param("size") int size);
    //查询客户总条数
    public int findAccountCount(@Param("managerId") long managerId);

    //查询咨询师账户余额
    public int doctorBalance(@Param("managerId") long managerId);

}
