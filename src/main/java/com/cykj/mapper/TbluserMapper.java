package com.cykj.mapper;

import com.cykj.bean.UserTb;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbluserMapper {

    //首先查询一下用户余额
    public List<UserTb> Balance(@Param("userId") long userId);

    //扣除相应的费用
    public int Spend(@Param("userId") long userId, @Param("balance") double balance);

    //扣款记录
    public int assessmentRecord(@Param("userId") long userId, @Param("managerId") long managerId, @Param("amount") double amount);



}
