package com.cykj.mapper;


import com.cykj.bean.Tblmanager;
import com.cykj.bean.UserTb;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblmanagerMapper {

    //后端 管理员或咨询师 登录
    public Tblmanager managerLogin(@Param("managerAcc") String managerAcc, @Param("managerPwd") String managerPwd);




    //客户管理，管理user表中的用户
    public List<UserTb> finduserList(@Param("userName") String userName,
                                       @Param("userSex") long userSex,
                                       @Param("userAge") long userAge,
                                       @Param("userPhone") String userPhone,
                                       @Param("userState") long userState,
                                       @Param("index") int index,
                                       @Param("size") int size);
    //查询客户总条数
    public int findUserCount(UserTb userTb);

    //启用、禁用
    public int updataUserState(@Param("userId") long userId);

    //删除客户信息
    public int updataUserMsg(@Param("userId") long userId );

    //重置密码
    public int updataUserPwd(@Param("userId") long userId );







    //后台用户管理
    public List<Tblmanager> findManagerList(@Param("managerId") long managerId,
                                     @Param("realName") String realName,
                                     @Param("titles") long titles,
                                     @Param("school") String school,
                                     @Param("managerState") long managerState,
                                     @Param("index") int index,
                                     @Param("size") int size);
    //查询后台用户总条数
    public int findManagerCount(Tblmanager tblmanager);

    //新增后台用户
    public int insertManager(Tblmanager tblmanager);


    //查询后台用户账号是否重复
    public int findManager(@Param("managerAcc") String managerAcc);


    //后台用户启用、禁用
    public int updataManagerState(@Param("managerId") long managerId);

    //删除后台用户信息
    public int updataManagerMsg(@Param("managerId") long managerId );

    //重置后台用户密码
    public int updataManagerPwd(@Param("managerId") long managerId );

    //通过id查询咨询师所有信息
    public List<Tblmanager> findDoctorById(@Param("managerId") long managerId );

    //修改tblmanager表咨询师余额
    public int updateManagerBalanceById(@Param("managerId") long managerId );


    public List<Tblmanager> findDoctorStateByAcc(@Param("managerAcc") String managerAcc );
}
