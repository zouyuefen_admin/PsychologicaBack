package com.cykj.mapper;


import com.cykj.bean.Tblpaper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblpaperMapper {

    //评估答题题目
    public List<Tblpaper> paper(@Param("areaId") long areaId,
                                @Param("index") int index,
                                @Param("size") int size);
    //评估答题条数
    public int paperCount(@Param("areaId") long areaId);

    //增加题目
    public int insertPaper(@Param("areaId") long areaId,
                           @Param("paperContent") String paperContent,
                           @Param("optionA") String optionA,
                           @Param("optionB") String optionB,
                           @Param("optionC") String optionC,
                           @Param("optionD") String optionD);


    //修改
    public int updatePaper(@Param("paperContent") String paperContent,
                           @Param("optionA") String optionA,
                           @Param("optionB") String optionB,
                           @Param("optionC") String optionC,
                           @Param("optionD") String optionD,
                           @Param("paperId") long paperId);

    //删除
    public int deletePaper(@Param("paperId") long paperId);

    //查tblpaper里面的所有
    public List<Tblpaper> list(@Param("paperId") long paperId);

}
