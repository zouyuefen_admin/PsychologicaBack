package com.cykj.mapper;

import com.cykj.bean.Tblappointment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblappointmentListMapper {
    //查询用户预约列表
    public List<Tblappointment> findAppointmentList(@Param("managerId") long managerId,
                                                    @Param("min") String min,
                                                    @Param("max") String max,
                                                    @Param("appointstate") String appointstate,
                                                    @Param("index") int index,
                                                    @Param("size") int size);

    //查询用户预约列表总条数（判断页码）
    public int findAppointmentListCount(@Param("managerId") long managerId,
                                        @Param("min") String min,
                                        @Param("max") String max,
                                        @Param("appointstate") String appointstate);

    //查询用户状态
    public List<Tblappointment> findUserState(@Param("managerId") long managerId);

    //改预约列表状态
    public int updateAppointstate(@Param("appointmentId") long appointmentId, @Param("appointstate") long appointstate);

    //预约详情
    public List<Tblappointment> bookDetail(@Param("appointmentId") long appointmentId);

    //诊断答复界面详情
    public List<Tblappointment> answer(@Param("appointmentId") long appointmentId);

    //咨询师诊断答复
    public int updateUserAnswer(@Param("appointmentId") long appointmentId, @Param("answer") String answer);


    //预约管理 预约列表
    public List<Tblappointment> findAppointmentLists(@Param("realName") String realName,
                                                     @Param("userName") String userName,
                                                     @Param("min") String min,
                                                     @Param("max") String max,
                                                     @Param("index") int index,
                                                     @Param("size") int size);

    //预约管理 预约列表总条数（判断页码）
    public int findAppointmentListCounts(@Param("realName") String realName,
                                         @Param("userName") String userName,
                                         @Param("min") String min,
                                         @Param("max") String max);

}
