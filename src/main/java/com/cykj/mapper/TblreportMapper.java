package com.cykj.mapper;



import com.cykj.bean.Tblreport;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface TblreportMapper {

    //查询报告
    public List<Tblreport> findReport(@Param("min") String min,
                                      @Param("max") String max,
                                      @Param("minScore") String minScore,
                                      @Param("maxScore") String maxScore,
                                      @Param("index") int index,
                                      @Param("size") int size);
    //查询报告总条数
    public int findReportCount(@Param("min") String min,
                               @Param("max") String max,
                               @Param("minScore") String minScore,
                               @Param("maxScore") String maxScore);



    //查询测评报告
    public List<Tblreport> findReportDetail(@Param("reportId") long reportId);
}
