package com.cykj.mapper;

import com.cykj.bean.Tblarea;
import com.cykj.bean.Tbldoctorarea;
import com.cykj.bean.Tblmanager;
import com.cykj.bean.Tblpower;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblareaMapper {
    //查找所有领域
    public List<Tblarea> findArea();

    //批量添加后台用户的领域
//    public int insertManagerArea(@Param("managerAcc") String managerAcc);


    //批量添加后台用户的领域
    public int insertManagerArea(@Param("managerAcc") String managerAcc,@Param("area") List<Integer> area);

}
