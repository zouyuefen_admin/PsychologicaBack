package com.cykj.mapper;

import com.cykj.bean.Tblpower;
import com.cykj.bean.Tblrole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblpowerMapper {

    //后端角色权限(判断是管理员还是咨询师)
    public List<Tblpower> findRolePower(@Param("roleid") long roleid);

    //查询系统中的角色列表
    public List<Tblrole> findRoleList();

    //根据角色id查询已分配的权限列表
    public List<Tblpower> findDistributedPower(@Param("roleid") long roleid);

    //根据角色id查询未分配的权限列表
    public List<Tblpower> findUnDistributedPower(@Param("roleid") long roleid);
    //根据角色id查询未分配的权限列表父级
    public List<Tblpower> findUnDistributedPower_parent(@Param("childList") List<Tblpower> childList);


    //1、添加权限
    public int addPower(@Param("roleid") long roleid,@Param("power") List<Integer> power);
    //2、添加所有权限
    public int addPowerAll(@Param("roleid") long roleid);

    //3、移除权限
    public int removePower(@Param("roleid") long roleid,@Param("power") List<Integer> power);

    //4、移除所有权限
    public int removePowerAll(@Param("roleid") long roleid);

    //查询所有一级标题
    public List<Tblpower> findParent();

}
