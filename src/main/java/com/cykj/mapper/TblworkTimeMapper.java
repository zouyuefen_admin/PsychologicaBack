package com.cykj.mapper;

import com.cykj.bean.Tblworktime;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblworkTimeMapper {
    //预约时间设置
    public int insertWorkTime(@Param("managerId") long managerId,@Param("workData") String workData, @Param("workTime") List<String> workTime);
    //回显（再次从数据库中查询时间）
    public List<Tblworktime> findWorkTime (@Param("managerId") long managerId);
    //动态修改（在选时间的时候，先将原本的记录删除，然后再调用上面的插入语句）
    public int deleteWorkTime(@Param("managerId") long managerId,@Param("workData") String workData);
}
