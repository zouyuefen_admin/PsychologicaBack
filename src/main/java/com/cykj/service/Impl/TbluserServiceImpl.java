package com.cykj.service.Impl;

import com.cykj.bean.UserTb;
import com.cykj.mapper.TbluserMapper;
import com.cykj.service.TbluserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TbluserServiceImpl implements TbluserService {
    @Autowired
    private TbluserMapper tbluserMapper;

    //查余额
    @Override
    public List<UserTb> Balance(long userId) {
        return tbluserMapper.Balance(userId);
    }

    //评估答题花费
    @Override
    public int Spend(long userId,double balance) {
        return tbluserMapper.Spend(userId,balance);
    }

    //插记录
    @Override
    public int assessmentRecord(long userId,long managerId,double amount) {
        return tbluserMapper.assessmentRecord(userId,managerId,amount);
    }


}
