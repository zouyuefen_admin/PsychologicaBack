package com.cykj.service.Impl;

import com.cykj.bean.Tblreport;
import com.cykj.mapper.TblreportMapper;
import com.cykj.service.TblreportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TblreportServiceImpl implements TblreportService {
    @Autowired
    private TblreportMapper tblreportMapper;

    //查询报告
    @Override
    public List<Tblreport> findReport(String min,
                                      String max,
                                      String minScore,
                                      String maxScore,
                                      int index,
                                      int size) {
        return tblreportMapper.findReport(min,max,minScore,maxScore,index,size);
    }
    //查询报告总条数
    @Override
    public int findReportCount(String min,
                               String max,
                               String minScore,
                               String maxScore) {
        return tblreportMapper.findReportCount(min,max,minScore,maxScore);
    }

    //查询测评报告
    @Override
    public List<Tblreport> findReportDetail(long reportId) {
        return tblreportMapper.findReportDetail(reportId);
    }
}
