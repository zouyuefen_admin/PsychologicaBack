package com.cykj.service.Impl;

import com.cykj.bean.Tblworktime;
import com.cykj.mapper.TblworkTimeMapper;
import com.cykj.service.TblworkTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TblworkTimeServiceImpl implements TblworkTimeService {
    @Autowired
    private TblworkTimeMapper tblworkTimeMapper;

    //插入设置时间
    @Override
    public int insertWorkTime(long managerId, String workData, List<String> workTime) {
        return tblworkTimeMapper.insertWorkTime(managerId,workData,workTime);
    }

    //查询设置时间
    @Override
    public List<Tblworktime> findWorkTime(long managerId) {
        return tblworkTimeMapper.findWorkTime(managerId);
    }

    //删除设置时间
    @Override
    public int deleteWorkTime(long managerId,String workData) {
        return tblworkTimeMapper.deleteWorkTime(managerId, workData);
    }
}
