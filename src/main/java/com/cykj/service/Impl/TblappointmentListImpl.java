package com.cykj.service.Impl;

import com.cykj.bean.Tblappointment;
import com.cykj.mapper.TblappointmentListMapper;
import com.cykj.service.TblappointmentListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TblappointmentListImpl implements TblappointmentListService {
    @Autowired
    private TblappointmentListMapper tblappointmentListMapper;


    @Override
    public List<Tblappointment> findAppointmentList(long managerId,String min, String max,String appointstate, int index, int size) {
        return tblappointmentListMapper.findAppointmentList(managerId, min,  max, appointstate,index,size);
    }

    @Override
    public int findAppointmentListCount(long managerId, String min,
                                        String max,
                                        String appointstate) {
        return tblappointmentListMapper.findAppointmentListCount(managerId,min,max,appointstate);
    }

    //查询用户状态
    @Override
    public List<Tblappointment> findUserState(long managerId) {
        return tblappointmentListMapper.findUserState(managerId);
    }

    @Override
    public int updateAppointstate(long appointmentId, long appointstate) {
        return tblappointmentListMapper.updateAppointstate(appointmentId,appointstate);
    }

    //预约详情
    @Override
    public List<Tblappointment> bookDetail(long appointmentId) {
        return tblappointmentListMapper.bookDetail(appointmentId);
    }

    //诊断答复
    @Override
    public List<Tblappointment> answer(long appointmentId) {
        return tblappointmentListMapper.answer(appointmentId);
    }

    //咨询师诊断答复
    @Override
    public int updateUserAnswer(long appointmentId, String answer) {
        return tblappointmentListMapper.updateUserAnswer(appointmentId,answer);
    }

    //预约管理 预约列表
    @Override
    public List<Tblappointment> findAppointmentLists(String realName, String userName, String min, String max, int index, int size) {
        return tblappointmentListMapper.findAppointmentLists(realName,userName,min,max,index,size);
    }

    //预约管理 预约列表总条数（判断页码）
    @Override
    public int findAppointmentListCounts(String realName, String userName, String min, String max) {
        return tblappointmentListMapper.findAppointmentListCounts(realName,userName,min,max);
    }
}
