package com.cykj.service.Impl;


import com.cykj.bean.Tblappointment;
import com.cykj.bean.UserTb;
import com.cykj.mapper.UserCountMapper;
import com.cykj.service.UserCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserCountServiceImpl implements UserCountService {
    @Autowired
    private UserCountMapper userCountMapper;


    //本周
    @Override
    public List<UserTb> findWeekList() {
        return userCountMapper.findWeekList();
    }

    //本月
    @Override
    public List<UserTb> findMonthList() {
        return userCountMapper.findMonthList();
    }

    //近半年
    @Override
    public List<UserTb> findhalfYearList(List<String> yearMonths) {
        return userCountMapper.findhalfYearList( yearMonths);
    }

    //渠道量統計
    @Override
    public List<Tblappointment> findDoctorAppointCount(String startDate,String endDate ) {
        return userCountMapper.findDoctorAppointCount(startDate,endDate);
    }

}
