package com.cykj.service.Impl;

import com.cykj.bean.Tblpower;
import com.cykj.bean.Tblrole;
import com.cykj.mapper.TblpowerMapper;
import com.cykj.service.TblpowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TblpowerServiceImpl implements TblpowerService {
    @Autowired
    private TblpowerMapper tblpowerMapper;

    //查询权限
    @Override
    public List<Tblpower> findRolePower(long roleid) {
        return tblpowerMapper.findRolePower(roleid);
    }

    @Override
    public List<Tblrole> findRoleList() {
        return tblpowerMapper.findRoleList();
    }

    //已拥有
    @Override
    public List<Tblpower> findDistributedPower(long roleid) {
        return tblpowerMapper.findDistributedPower(roleid);
    }

    //为拥有
    @Override
    public List<Tblpower> findUnDistributedPower(long roleid) {
        return tblpowerMapper.findUnDistributedPower(roleid);
    }


    //添加权限
    @Override
    public int addPower(long roleid, List<Integer> power) {
        //提前做移除
        tblpowerMapper.removePower(roleid,power);
        return tblpowerMapper.addPower(roleid,power);
    }



    //添加所有权限
    @Override
    public int addPowerAll(long roleid) {
        //提前做移除
//        tblpowerMapper.removePowerAll(roleid);
        //再添加
        return tblpowerMapper.addPowerAll(roleid);
    }

    //移除权限
    @Override
    public int removePower(long roleid,List<Integer> power) {
        return tblpowerMapper.removePower(roleid,power);
    }

    //移除所有权限
    @Override
    public int removePowerAll(long roleid) {
        return tblpowerMapper.removePowerAll(roleid);
    }

    //查找所有一级标题
    @Override
    public List<Tblpower> findParent() {
        return tblpowerMapper.findParent();
    }


}
