package com.cykj.service.Impl;


import com.cykj.bean.Tblbill;
import com.cykj.mapper.TblbillMapper;
import com.cykj.service.TblbillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TblbillServiceImpl implements TblbillService {
    @Autowired
    private TblbillMapper tblbillMapper;


    @Override
    public List<Tblbill> findAccountList(long managerId, int index, int size) {
        return tblbillMapper.findAccountList(managerId,index,size);
    }

    @Override
    public int findAccountCount(long managerId) {
        return tblbillMapper.findAccountCount(managerId);
    }

    @Override
    public int doctorBalance(long managerId) {
        return tblbillMapper.doctorBalance(managerId);
    }
}
