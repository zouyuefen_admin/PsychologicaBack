package com.cykj.service.Impl;


import com.cykj.bean.Tblmanager;
import com.cykj.bean.UserTb;
import com.cykj.mapper.TblmanagerMapper;
import com.cykj.service.TblmanagerService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TblmanagerServiceImpl implements TblmanagerService {
    @Autowired
    private TblmanagerMapper tblmanagerMapper;

    //后端 管理员或咨询师 登录
    @Override
    public Tblmanager managerLogin(String managerAcc, String managerPwd) {
        return tblmanagerMapper.managerLogin(managerAcc,managerPwd);
    }


    //客户管理
    @Override
    public List<UserTb> finduserList(String userName, long userSex, long userAge, String userPhone, long userState, int index, int size) {
        return tblmanagerMapper.finduserList(userName,userSex,userAge,userPhone,userState,index,size);
    }

    //查询客户总条数
    @Override
    public int findUserCount(UserTb userTb) {
        return tblmanagerMapper.findUserCount(userTb);
    }

    //更新禁用、启动状态
    @Override
    public int updataUserState(long userId) {
        return tblmanagerMapper.updataUserState(userId);
    }

    //删除客户信息
    @Override
    public int delUserMsg(long userId) {
        return tblmanagerMapper.updataUserMsg(userId);
    }

    //重置密码
    @Override
    public int updataUserPwd(long userId) {
        return tblmanagerMapper.updataUserPwd(userId);
    }



    //后台用户管理
    @Override
    public List<Tblmanager> findManagerList(long managerId, String realName, long titles, String school, long managerState, int index, int size) {
        return tblmanagerMapper.findManagerList(managerId,realName,titles,school,managerState,index,size);
    }

    //查询后台用户总条数
    @Override
    public int findManagerCount(Tblmanager tblmanager) {
        return tblmanagerMapper.findManagerCount(tblmanager);
    }

    //新增后台用户
    @Override
    public int insertManager(Tblmanager tblmanager) {
        return tblmanagerMapper.insertManager(tblmanager);
    }



    @Override
    public int findManager(String managerAcc) {
        return tblmanagerMapper.findManager(managerAcc);
    }

    //后台用户启用、禁用
    @Override
    public int updataManagerState(long managerId) {
        return tblmanagerMapper.updataManagerState(managerId);
    }

    //删除后台用户信息
    @Override
    public int updataManagerMsg(long managerId) {
        return tblmanagerMapper.updataManagerMsg(managerId);
    }

    //重置后台用户密码
    @Override
    public int updataManagerPwd(long managerId) {
        return tblmanagerMapper.updataManagerPwd(managerId);
    }

    //通过id查询咨询师所有信息
    @Override
    public List<Tblmanager> findDoctorById(long managerId) {
        return tblmanagerMapper.findDoctorById(managerId);
    }


    //修改tblmanager表咨询师余额
    @Override
    public int updateManagerBalanceById(long managerId) {
        return tblmanagerMapper.updateManagerBalanceById(managerId);
    }

    //查状态
    @Override
    public List<Tblmanager> findDoctorStateByAcc(String managerAcc) {
        return tblmanagerMapper.findDoctorStateByAcc(managerAcc);
    }

}
