package com.cykj.service.Impl;

import com.cykj.bean.Tblpaper;
import com.cykj.mapper.TblpaperMapper;
import com.cykj.service.TblpaperService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TblpaperServiceImpl implements TblpaperService {
    @Autowired
    private TblpaperMapper tblpaperMapper;

    //查询
    @Override
    public List<Tblpaper> paper(long areaId,int index, int size) {
        return tblpaperMapper.paper(areaId,index,size);
    }

    //查询条数
    @Override
    public int paperCount(long areaId) {
        return tblpaperMapper.paperCount(areaId);
    }

    //插入
    @Override
    public int insertPaper(long areaId, String paperContent, String optionA, String optionB, String optionC, String optionD) {
        return tblpaperMapper.insertPaper(areaId,paperContent,optionA,optionB,optionC,optionD);
    }

    //修改
    @Override
    public int updatePaper(String paperContent, String optionA, String optionB, String optionC, String optionD, long paperId) {
        return tblpaperMapper.updatePaper(paperContent,optionA,optionB,optionC,optionD,paperId);
    }

    //删除
    @Override
    public int deletePaper(long paperId) {
        return tblpaperMapper.deletePaper(paperId);
    }

    //查tblpaper里面的所有
    @Override
    public List<Tblpaper> list(long paperId) {
        return tblpaperMapper.list(paperId);
    }




}
