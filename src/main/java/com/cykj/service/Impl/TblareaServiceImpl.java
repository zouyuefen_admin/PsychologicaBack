package com.cykj.service.Impl;

import com.cykj.bean.Tblarea;
import com.cykj.mapper.TblareaMapper;
import com.cykj.mapper.TblbillMapper;
import com.cykj.service.TblareaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TblareaServiceImpl implements TblareaService {
    @Autowired
    private TblareaMapper tblareaMapper;

    @Override
    public List<Tblarea> findArea() {
        return tblareaMapper.findArea();
    }

    @Override
    public int insertManagerArea(String managerAcc, List<Integer> area) {
        return tblareaMapper.insertManagerArea(managerAcc,area);
    }

//    @Override
//    public int insertManagerArea(String managerAcc) {
//        return tblareaMapper.insertManagerArea(managerAcc);
//    }


}
