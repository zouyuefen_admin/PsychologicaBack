package com.cykj.service;

import com.cykj.bean.Tblworktime;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblworkTimeService {

    //插入设置时间
    public int insertWorkTime( long managerId, String workData,  List<String> workTime);
    //回显（再次从数据库中查询时间）
    public List<Tblworktime> findWorkTime (long managerId);
    //动态修改（在选时间的时候，先将原本的记录删除，然后再调用上面的插入语句）
    public int deleteWorkTime(long managerId,String workData);

}
