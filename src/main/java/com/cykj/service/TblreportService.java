package com.cykj.service;

import com.cykj.bean.Tblreport;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblreportService {
    //查询报告
    public List<Tblreport> findReport(String min,
                                      String max,
                                      String minScore,
                                      String maxScore,
                                      int index,
                                      int size);

    //查询报告总条数
    public int findReportCount(String min,
                               String max,
                               String minScore,
                               String maxScore);

    //查询测评报告
    public List<Tblreport> findReportDetail(long reportId);
}
