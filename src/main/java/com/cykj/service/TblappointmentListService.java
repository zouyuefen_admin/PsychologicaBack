package com.cykj.service;

import com.cykj.bean.Tblappointment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblappointmentListService {
    //查询用户预约列表
    public List<Tblappointment> findAppointmentList(long managerId, String min, String max, String appointstate, int index, int size);

    //查询用户预约列表总条数（判断页码）
    public int findAppointmentListCount(long managerId,
                                        String min,
                                        String max,
                                        String appointstate);

    //查询用户状态
    public List<Tblappointment> findUserState(long managerId);

    //改预约列表状态
    public int updateAppointstate(long appointmentId, long appointstate);

    //预约详情
    public List<Tblappointment> bookDetail(long appointmentId);

    //诊断答复界面详情
    public List<Tblappointment> answer(long appointmentId);

    //咨询师诊断答复
    public int updateUserAnswer(long appointmentId, String answer);


    //预约管理 预约列表
    public List<Tblappointment> findAppointmentLists(String realName,
                                                     String userName,
                                                     String min,
                                                     String max,
                                                     int index,
                                                     int size);

    //预约管理 预约列表总条数（判断页码）
    public int findAppointmentListCounts(String realName,
                                         String userName,
                                         String min,
                                         String max);

}
