package com.cykj.service;

import com.cykj.bean.Tblappointment;
import com.cykj.bean.UserTb;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserCountService {

    //统计本周新增用户数
    public List<UserTb> findWeekList();

    //统计本月新增用户数
    public List<UserTb> findMonthList();

    //统计近半年新增用户数
    public List<UserTb> findhalfYearList(List<String> yearMonths);

    //渠道量統計
    public  List<Tblappointment> findDoctorAppointCount(String startDate,String endDate);

}
