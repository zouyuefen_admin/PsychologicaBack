package com.cykj.service;




import com.cykj.bean.Tblmanager;
import com.cykj.bean.Tblpower;
import com.cykj.bean.UserTb;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface TblmanagerService {

    //后端 管理员或咨询师 登录
    public Tblmanager managerLogin(String managerAcc, String managerPwd);


    //客户管理，管理user表中的用户
    public List<UserTb> finduserList(String userName,
                                     long userSex,
                                     long userAge,
                                     String userPhone,
                                     long userState,
                                     int index,
                                     int size);

    //查询客户总条数
    public int findUserCount(UserTb userTb);

    //启用、禁用
    public int updataUserState(long userId);

    //删除客户信息
    public int delUserMsg(long userId);

    //重置密码
    public int updataUserPwd(long userId);



    //后台用户管理
    public List<Tblmanager> findManagerList(long managerId,
                                            String realName,
                                            long titles,
                                            String school,
                                            long managerState,
                                            int index,
                                            int size);

    //查询后台用户总条数
    public int findManagerCount(Tblmanager tblmanager);

    //新增后台用户
    public int insertManager(Tblmanager tblmanager);


    //查询后台用户账号是否重复
    public int findManager(String managerAcc);

    //后台用户启用、禁用
    public int updataManagerState(long managerId);

    //删除后台用户信息
    public int updataManagerMsg(long managerId);

    //重置后台用户密码
    public int updataManagerPwd(long managerId);

    //通过id查询咨询师所有信息
    public List<Tblmanager> findDoctorById(long managerId );

    //修改tblmanager表咨询师余额
    public int updateManagerBalanceById(long managerId );

    //查状态
    public List<Tblmanager> findDoctorStateByAcc(@Param("managerAcc") String managerAcc );
}
