package com.cykj.service;

import com.cykj.bean.Tblarea;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblareaService {
    //查找所以领域
    public List<Tblarea> findArea();

//    //新增后台用户的领域
//    public int insertManagerArea(@Param("managerAcc") String managerAcc);

    //批量添加后台用户的领域
    public int insertManagerArea(String managerAcc,List<Integer> area);

}
