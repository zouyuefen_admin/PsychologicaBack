package com.cykj.service;

import com.cykj.bean.Tblpower;
import com.cykj.bean.Tblrole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblpowerService {
    //查询权限
    public List<Tblpower> findRolePower(long roleid);

    //查询系统中的角色列表
    public List<Tblrole> findRoleList();

    //根据角色id查询已分配的权限列表
    public List<Tblpower> findDistributedPower(long roleid);

    //根据角色id查询未分配的权限列表
    public List<Tblpower> findUnDistributedPower(long roleid);


    //添加权限
    public int addPower(long roleid, List<Integer> power);

    //添加所有权限
    public int addPowerAll(long roleid);

    //移除权限
    public int removePower(long roleid,List<Integer> power);

    //移除所有权限
    public int removePowerAll(long roleid);

    //查询所有一级标题
    public List<Tblpower> findParent();
}
