package com.cykj.service;

import com.cykj.bean.Tblpaper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblpaperService {
    //评估答题题目
    public List<Tblpaper> paper(long areaId,int index, int size);

    //评估答题题目条数
    public int paperCount(long areaId);

    //增加题目
    public int insertPaper(long areaId,
                           String paperContent,
                            String optionA,
                            String optionB,
                            String optionC,
                            String optionD);


    //修改
    public int updatePaper( String paperContent,
                            String optionA,
                            String optionB,
                            String optionC,
                            String optionD,
                            long paperId);

    //删除
    public int deletePaper(long paperId);

    //查tblpaper里面的所有
    public List<Tblpaper> list(long paperId);
}
