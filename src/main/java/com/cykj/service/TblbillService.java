package com.cykj.service;


import com.cykj.bean.Tblbill;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblbillService {

    //查找资金账户列表
    public List<Tblbill> findAccountList(long managerId,
                                         int index,
                                         int size);

    //查询客户总条数
    public int findAccountCount(long managerId);

    //查询咨询师账户余额
    public int doctorBalance(long managerId);
}
