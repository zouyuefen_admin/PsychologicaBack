package com.cykj.service;

import com.cykj.bean.UserTb;

import java.util.List;

public interface TbluserService {

    //首先查询一下用户余额
    public List<UserTb> Balance(long userId);

    //扣除相应的费用user表
    public int Spend(long userId, double balance);

    //插记录 记录bill表
    public int assessmentRecord(long userId, long managerId, double amount);

}
