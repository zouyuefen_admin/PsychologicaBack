package com.cykj.bean;

public class Tblreport {

  private long reportId;
  private long userId;
  private long areaId;
  private String reportDate;
  private long reportScore;
  private String reportContent;
  private String reportResults;
  private String min;
  private String max;
  private long minScore;
  private long maxScore;
  private String userName;
  private String areaName;


  public String getAreaName() {
    return areaName;
  }

  public void setAreaName(String areaName) {
    this.areaName = areaName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getMin() {
    return min;
  }

  public void setMin(String min) {
    this.min = min;
  }

  public String getMax() {
    return max;
  }

  public void setMax(String max) {
    this.max = max;
  }

  public long getMinScore() {
    return minScore;
  }

  public void setMinScore(long minScore) {
    this.minScore = minScore;
  }

  public long getMaxScore() {
    return maxScore;
  }

  public void setMaxScore(long maxScore) {
    this.maxScore = maxScore;
  }

  public long getAreaId() {
    return areaId;
  }

  public void setAreaId(long areaId) {
    this.areaId = areaId;
  }

  public long getReportId() {
    return reportId;
  }

  public void setReportId(long reportId) {
    this.reportId = reportId;
  }


  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }


  public String getReportDate() {
    return reportDate;
  }

  public void setReportDate(String reportDate) {
    this.reportDate = reportDate;
  }


  public long getReportScore() {
    return reportScore;
  }

  public void setReportScore(long reportScore) {
    this.reportScore = reportScore;
  }


  public String getReportContent() {
    return reportContent;
  }

  public void setReportContent(String reportContent) {
    this.reportContent = reportContent;
  }


  public String getReportResults() {
    return reportResults;
  }

  public void setReportResults(String reportResults) {
    this.reportResults = reportResults;
  }

}
