package com.cykj.bean;


public class Tblworktime {

  private long workId;
  private long managerId;
  private String workTime;
  private String workData;


  public long getWorkId() {
    return workId;
  }

  public void setWorkId(long workId) {
    this.workId = workId;
  }


  public long getManagerId() {
    return managerId;
  }

  public void setManagerId(long managerId) {
    this.managerId = managerId;
  }


  public String getWorkTime() {
    return workTime;
  }

  public void setWorkTime(String workTime) {
    this.workTime = workTime;
  }

  public String getWorkData() {
    return workData;
  }

  public void setWorkData(String workData) {
    this.workData = workData;
  }
}
