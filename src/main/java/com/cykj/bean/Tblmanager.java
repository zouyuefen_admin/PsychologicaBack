package com.cykj.bean;


public class Tblmanager {

  private long managerId;
  private long roleid;
  private String managerAcc;
  private String managerPwd;
  private String realName;
  private long titles;//职称id
  private String school;
  private long managerState;
  private long managerBalance;
  private String backGround;
  private long price;
  private String introduction;

  public String getIntroduction() {
    return introduction;
  }

  public void setIntroduction(String introduction) {
    this.introduction = introduction;
  }

  public long getManagerBalance() {
    return managerBalance;
  }

  public void setManagerBalance(long managerBalance) {
    this.managerBalance = managerBalance;
  }

  public long getManagerId() {
    return managerId;
  }

  public void setManagerId(long managerId) {
    this.managerId = managerId;
  }


  public long getRoleid() {
    return roleid;
  }

  public void setRoleid(long roleid) {
    this.roleid = roleid;
  }

  public String getManagerAcc() {
    return managerAcc;
  }

  public void setManagerAcc(String managerAcc) {
    this.managerAcc = managerAcc;
  }


  public String getManagerPwd() {
    return managerPwd;
  }

  public void setManagerPwd(String managerPwd) {
    this.managerPwd = managerPwd;
  }


  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  public long getTitles() {
    return titles;
  }

  public void setTitles(long titles) {
    this.titles = titles;
  }

  public String getSchool() {
    return school;
  }

  public void setSchool(String school) {
    this.school = school;
  }


  public long getManagerState() {
    return managerState;
  }

  public void setManagerState(long managerState) {
    this.managerState = managerState;
  }

  public String getBackGround() {
    return backGround;
  }

  public void setBackGround(String backGround) {
    this.backGround = backGround;
  }

  public long getPrice() {
    return price;
  }

  public void setPrice(long price) {
    this.price = price;
  }
}
