package com.cykj.bean;


public class Tblappointment {

  private long appointmentId;
  private long userId;
  private long areaId;
  private long managerId;
  private String appointmentDate;
  private String appointmentTime;
  private String appointstate;
  private String problem;
  private String answer;
  private String replydate;
  private double appointCost;
  private String evaluate;
  private String userName;
  private String areaName;
  private String realName;
  private String min;
  private String max;
  private long price;

  private long appointCounts;


  public long getAppointCounts() {
    return appointCounts;
  }

  public void setAppointCounts(long appointCounts) {
    this.appointCounts = appointCounts;
  }


  public long getPrice() {
    return price;
  }

  public void setPrice(long price) {
    this.price = price;
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  public String getMin() {
    return min;
  }

  public void setMin(String min) {
    this.min = min;
  }

  public String getMax() {
    return max;
  }

  public void setMax(String max) {
    this.max = max;
  }

  public long getManagerId() {
    return managerId;
  }

  public void setManagerId(long managerId) {
    this.managerId = managerId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getAreaName() {
    return areaName;
  }

  public void setAreaName(String areaName) {
    this.areaName = areaName;
  }




  public long getAppointmentId() {
    return appointmentId;
  }

  public void setAppointmentId(long appointmentId) {
    this.appointmentId = appointmentId;
  }


  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }


  public long getAreaId() {
    return areaId;
  }

  public void setAreaId(long areaId) {
    this.areaId = areaId;
  }


  public long getDoctorId() {
    return managerId;
  }

  public void setDoctorId(long doctorId) {
    this.managerId = managerId;
  }


  public String getAppointstate() {
    return appointstate;
  }

  public void setAppointstate(String appointstate) {
    this.appointstate = appointstate;
  }


  public String getProblem() {
    return problem;
  }

  public void setProblem(String problem) {
    this.problem = problem;
  }


  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }


  public double getAppointCost() {
    return appointCost;
  }

  public void setAppointCost(double appointCost) {
    this.appointCost = appointCost;
  }


  public String getEvaluate() {
    return evaluate;
  }

  public void setEvaluate(String evaluate) {
    this.evaluate = evaluate;
  }

  public String getReplydate() {
    return replydate;
  }

  public void setReplydate(String replydate) {
    this.replydate = replydate;
  }

  public String getAppointmentDate() {
    return appointmentDate;
  }

  public void setAppointmentDate(String appointmentDate) {
    this.appointmentDate = appointmentDate;
  }

  public String getAppointmentTime() {
    return appointmentTime;
  }

  public void setAppointmentTime(String appointmentTime) {
    this.appointmentTime = appointmentTime;
  }
}
