package com.cykj.bean;


public class Tblpaper {

  private long onlineId;
  private long paperId;
  private long areaId;
  private String paperContent;
  private double paperscore;
  private String optionA;
  private String optionB;
  private String optionC;
  private String optionD;


  public long getOnlineId() {
    return onlineId;
  }

  public void setOnlineId(long onlineId) {
    this.onlineId = onlineId;
  }


  public long getPaperId() {
    return paperId;
  }

  public void setPaperId(long paperId) {
    this.paperId = paperId;
  }


  public long getAreaId() {
    return areaId;
  }

  public void setAreaId(long areaId) {
    this.areaId = areaId;
  }


  public String getPaperContent() {
    return paperContent;
  }

  public void setPaperContent(String paperContent) {
    this.paperContent = paperContent;
  }


  public double getPaperscore() {
    return paperscore;
  }

  public void setPaperscore(double paperscore) {
    this.paperscore = paperscore;
  }

  public String getOptionA() {
    return optionA;
  }

  public void setOptionA(String optionA) {
    this.optionA = optionA;
  }

  public String getOptionB() {
    return optionB;
  }

  public void setOptionB(String optionB) {
    this.optionB = optionB;
  }

  public String getOptionC() {
    return optionC;
  }

  public void setOptionC(String optionC) {
    this.optionC = optionC;
  }

  public String getOptionD() {
    return optionD;
  }

  public void setOptionD(String optionD) {
    this.optionD = optionD;
  }
}
