package com.cykj.bean;


public class Tblbill {

  private long billId;
  private long userId;
  private long managerId;
  private String billdate;
  private long billtype;
  private String state;
  private double amount;
  private String userName;


  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public long getBillId() {
    return billId;
  }

  public void setBillId(long billId) {
    this.billId = billId;
  }


  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }


  public String getBilldate() {
    return billdate;
  }

  public void setBilldate(String billdate) {
    this.billdate = billdate;
  }

  public long getManagerId() {
    return managerId;
  }

  public void setManagerId(long managerId) {
    this.managerId = managerId;
  }

  public long getBilltype() {
    return billtype;
  }

  public void setBilltype(long billtype) {
    this.billtype = billtype;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }


  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

}
