package com.cykj.bean;


public class Tbldoctorarea {

  private long doctorAreaId;
  private String managerAcc;
  private long areaid;


  public long getDoctorAreaId() {
    return doctorAreaId;
  }

  public void setDoctorAreaId(long doctorAreaId) {
    this.doctorAreaId = doctorAreaId;
  }

  public String getManagerAcc() {
    return managerAcc;
  }

  public void setManagerAcc(String managerAcc) {
    this.managerAcc = managerAcc;
  }

  public long getAreaid() {
    return areaid;
  }

  public void setAreaid(long areaid) {
    this.areaid = areaid;
  }

}
