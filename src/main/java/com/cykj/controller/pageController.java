package com.cykj.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class pageController {

    //验证码
    @RequestMapping("/code1")
    public String code() {
        return "code";
    }

    //登入
    @RequestMapping("/login")
    public String login() {
        return "login";
    }


    @RequestMapping("/home")
    public String home() {
        return "home";
    }

    //管理界面
    @RequestMapping("/managePage")
    public String managePage() {
        return "managePage";
    }

    //用户管理
    @RequestMapping("/userManager")
    public String userManager() {
        return "userManager";
    }


    //后台用户管理
    @RequestMapping("/backUserManager")
    public String backUserManager() {
        return "backUserManager";
    }

    //登出
    @RequestMapping("/exit")
    public String exit(HttpSession session) {
        session.removeAttribute("user");
        return "login";
    }

    //资金账户
    @RequestMapping("/bill")
    public String account() {
        return "bill";
    }

    //预约时间设置
    @RequestMapping("/dateTime")
    public String dateTime() {
        return "dateTime";
    }

    //角色权限
    @RequestMapping("/rolePower")
    public String rolePower() {
        return "rolePower";
    }

    //初始界面
    @RequestMapping("/start")
    public String start() {
        return "start";
    }

    //预约列表
    @RequestMapping("/appointmentList")
    public String appointmentList() {
        return "appointmentList";
    }

    //预约管理界面
    @RequestMapping("/bookingManagement")
    public String bookingManagement() {
        return "bookingManagement";
    }

    //在线评估管理界面
    @RequestMapping("/onlineAssessmentMnagement")
    public String onlineAssessmentMnagement() {
        return "onlineAssessmentMnagement";
    }

    //题库管理
    @RequestMapping("/questionBank")
    public String questionBank() {
        return "questionBank";
    }

    //用户统计
    @RequestMapping("/userStatistics")
    public String userStatistics() {
        return "userStatistics";
    }

    //渠道量统计
    @RequestMapping("/channelStatistics")
    public String channelStatistics() {
        return "channelStatistics";
    }

    //预约详情
    @RequestMapping("/book")
    public String book() {
        return "book";
    }

    //诊断答复
    @RequestMapping("/answer")
    public String answer() {
        return "answer";
    }

    //评测报告
    @RequestMapping("/reportDetail")
    public String reportDetail() {
        return "reportDetail";
    }

}
