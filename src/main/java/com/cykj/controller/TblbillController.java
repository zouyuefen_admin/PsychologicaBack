package com.cykj.controller;

import com.alibaba.fastjson.JSON;
import com.cykj.bean.Tblbill;
import com.cykj.bean.Tblmanager;
import com.cykj.service.TblbillService;
import com.cykj.service.TblmanagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/Accounts", produces = "text/html;charset=utf-8")
public class TblbillController {
    @Autowired
    private TblbillService tblbillService;

    @RequestMapping("/billList")
    @ResponseBody
    public String billList(int page,int size,HttpSession session){
        Tblmanager tblmanager = (Tblmanager) session.getAttribute("user");
        //页码起始值为：（页码-1）*当页条数
        page=(page-1)*size;
        //查询资金账户列表
        List<Tblbill> accountList = tblbillService.findAccountList(tblmanager.getManagerId(), page, size);
        int total = tblbillService.findAccountCount(tblmanager.getManagerId());
        //用map存对象
        Map<String,Object> map = new HashMap<>();
        map.put("data",accountList);
        map.put("total",total);
        String json = JSON.toJSONString(map);
        return json;

    }


    //查询咨询师账户余额
    @RequestMapping("/balance")
    @ResponseBody
    public String balance( HttpSession session){
        Tblmanager tblmanager = (Tblmanager) session.getAttribute("user");
        //查询咨询师账户余额
        int balances = tblbillService.doctorBalance( tblmanager.getManagerId());
        System.out.println("咨询师账户余额为"+balances);
        String json = JSON.toJSONString(balances);
        return json;
    }
}
