package com.cykj.controller;


import com.alibaba.fastjson.JSON;

import com.alibaba.fastjson.JSONObject;
import com.cykj.bean.Tblmanager;
import com.cykj.bean.Tblpower;
import com.cykj.bean.Tblrole;
import com.cykj.service.TblpowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/power", produces = "text/html;charset=utf-8")
public class TblpowerController {
    @Autowired
    private TblpowerService tblpowerService;

    //查询后台登入的人是咨询师还是管理员
    @RequestMapping("/powerList")
    @ResponseBody
    public String powerList(HttpSession session){
        Tblmanager tblmanager = (Tblmanager) session.getAttribute("user");
        List<Tblpower> rolePower = tblpowerService.findRolePower(tblmanager.getRoleid());
        return JSON.toJSONString(rolePower);
    }

    //查询角色
    @RequestMapping("/roleList")
    @ResponseBody
    public String roleList(){
        List<Tblrole> roleList = tblpowerService.findRoleList();
        return JSON.toJSONString(roleList);

    }

    //查找已拥有权限和未拥有权限
    @RequestMapping("/findPowerList")
    @ResponseBody
    public String distributedPower(long roleid){
        List<Tblpower> distributedPower = tblpowerService.findDistributedPower(roleid);
        List<Tblpower> unDistributedPower = tblpowerService.findUnDistributedPower(roleid);
        Map<String,Object> map = new HashMap<>();
        map.put("haveList",distributedPower);
        map.put("unHaveList",unDistributedPower);
        return JSON.toJSONString(map);

    }


    //添加权限
    @RequestMapping("/addPower")
    @ResponseBody
    public String addPower(long roleid, String powers) {
        List<Integer> list = JSONObject.parseArray(powers).toJavaList(Integer.class);
        for (Integer a : list) {
            System.out.println(a);
        }
        int i = tblpowerService.addPower(roleid,list);
        System.out.println("批量添加了："+i+" 条权限");

        return String.valueOf(i);
    }


    //添加所有权限
    @RequestMapping("/addPowerAll")
    @ResponseBody
    public String addPowerAll(long roleid) {
        int i = tblpowerService.addPowerAll(roleid);
        return String.valueOf(i);
    }


    //移除权限
    @RequestMapping("/removePower")
    @ResponseBody
    public String removePower(long roleid,String powers) {
        List<Integer> list = JSONObject.parseArray(powers).toJavaList(Integer.class);
        for (Integer a : list) {
            System.out.println(a);
        }
        int i = tblpowerService.removePower(roleid,list);
        return String.valueOf(i);
    }


    //移除所有权限
    @RequestMapping("/removerPowerAll")
    @ResponseBody
    public String removerPowerAll(long roleid){
        int i = tblpowerService.removePowerAll(roleid);
        return String.valueOf(i);
    }

    //查询所有一级标题
    @RequestMapping("/findParent")
    @ResponseBody
    public String findParent(){
        List<Tblpower> parent = tblpowerService.findParent();
        return JSON.toJSONString(parent);
    }
}
