package com.cykj.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cykj.bean.Tblarea;
import com.cykj.bean.Tblmanager;
import com.cykj.bean.Tblpower;
import com.cykj.service.TblareaService;
import com.cykj.service.TblmanagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
@RequestMapping(value = "/Area", produces = "text/html;charset=utf-8")
public class TblAreaController {
        @Autowired
        private TblareaService tblareaService;

        //查找所有领域
        @RequestMapping("/areaList")
        @ResponseBody
        public String areaList(){
            List<Tblarea> areaList = tblareaService.findArea();
            return JSON.toJSONString(areaList);
    }

    //新用户添加领域(咨询师)
    @RequestMapping("/addDoctorArea")
    @ResponseBody
    public String powerList(String managerAcc,String areas) {
        List<Integer> list = JSONObject.parseArray(areas).toJavaList(Integer.class);
        for (Integer a : list) {
            System.out.println(a);
        }

        int i = tblareaService.insertManagerArea(managerAcc,list);
        System.out.println("设置领域" + i + "条");
        return i + "";
    }
}
