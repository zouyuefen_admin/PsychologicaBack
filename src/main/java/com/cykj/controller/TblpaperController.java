package com.cykj.controller;

import com.alibaba.fastjson.JSON;
import com.cykj.bean.Tblappointment;
import com.cykj.bean.Tblmanager;
import com.cykj.bean.Tblpaper;
import com.cykj.service.TblpaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = "/paper", produces = "text/html;charset=utf-8")
public class TblpaperController {

    @Autowired
    private TblpaperService tblpaperService;

    //评估答题
    @RequestMapping("/list")
    @ResponseBody
    public String findPaper(long areaId,int page,int size){
        //页码起始值为：（页码-1）*当页条数
        page=(page-1)*size;
        //查询题库
        List<Tblpaper> tblpaperList = tblpaperService.paper(areaId,page, size);
        int total = tblpaperService.paperCount(areaId);
        //用map存对象
        Map<String,Object> map = new HashMap<>();
        map.put("data",tblpaperList);
        map.put("total",total);
        String json = JSON.toJSONString(map);
        return json;
    }

    //添加
    @RequestMapping("/insertPaper")
    @ResponseBody
    public String insertPaper(long areaId, String paperContent, String optionA, String optionB, String optionC, String optionD) {
        int a = tblpaperService.insertPaper(areaId,paperContent,optionA,optionB,optionC,optionD);
        return String.valueOf(a);
    }

    //修改
    @RequestMapping("/updatePaper")
    @ResponseBody
    public String updatePaper(String paperContent, String optionA, String optionB, String optionC, String optionD, long paperId) {
        int a =tblpaperService.updatePaper(paperContent,optionA,optionB,optionC,optionD,paperId);
        return String.valueOf(a);
    }

    //删除
    @RequestMapping("/deletePaper")
    @ResponseBody
    public String deletePaper(long paperId) {
        int a =tblpaperService.deletePaper(paperId);
        return String.valueOf(a);
    }


    //查所有
    @RequestMapping("/paperList")
    @ResponseBody
    public String paperList(long paperId) {
        List<Tblpaper> list = tblpaperService.list(paperId);
        return JSON.toJSONString(list);
    }



}
