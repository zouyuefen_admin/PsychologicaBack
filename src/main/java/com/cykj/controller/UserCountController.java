package com.cykj.controller;

import com.alibaba.fastjson.JSON;
import com.cykj.bean.Tblappointment;
import com.cykj.bean.UserTb;
import com.cykj.service.UserCountService;
import com.cykj.utils.YearUtis;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/userCount", produces = "text/html;charset=utf-8")
public class UserCountController {
    @Autowired
    private UserCountService userCountService;


    //本周
    @RequestMapping("/findWeekList")
    @ResponseBody
    public String findWeekList() {
        List<UserTb> weekList = userCountService.findWeekList();
        return JSON.toJSONString(weekList);
    }

    //本月
    @RequestMapping("/findMonthList")
    @ResponseBody
    public String findMonthList() {
        List<UserTb> monthList = userCountService.findMonthList();
        return JSON.toJSONString(monthList);
    }

    //近半年
    @RequestMapping("/findhalfYearList")
    @ResponseBody
    public String findhalfYearList() {
        List<String> list = YearUtis.halfYear();
        List<UserTb> userTbList = userCountService.findhalfYearList(list);
        return JSON.toJSONString(userTbList);
    }


    //渠道量統計
    @RequestMapping("/doctorAppointCount")
    @ResponseBody
    public String channelStatistics(String startDate,String endDate) {
        List<Tblappointment> doctorAppointCount = userCountService.findDoctorAppointCount(startDate, endDate);
        return JSON.toJSONString(doctorAppointCount);
    }
}

