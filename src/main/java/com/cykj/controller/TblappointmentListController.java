package com.cykj.controller;

import com.alibaba.fastjson.JSON;
import com.cykj.bean.Tblappointment;
import com.cykj.bean.Tblmanager;
import com.cykj.bean.UserTb;
import com.cykj.service.TblappointmentListService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/userAppointList", produces = "text/html;charset=utf-8")
public class TblappointmentListController {

    @Autowired
    private TblappointmentListService tblappointmentListService;

    //查询预约列表
    @RequestMapping("/List")
    @ResponseBody
    public String findList(int page,int size,HttpSession session, String min, String max, String appointstate){
        Tblmanager tblmanager = (Tblmanager) session.getAttribute("user");

        System.out.println("有拿到用户的账号："+tblmanager.getManagerId());

        //页码起始值为：（页码-1）*当页条数
        //        page=(page-1)*size;
        //        //查询预约列表
        List<Tblappointment> appointmentList = tblappointmentListService.findAppointmentList(tblmanager.getManagerId(),min,max, appointstate,page, size);
        int total = tblappointmentListService.findAppointmentListCount(tblmanager.getManagerId(),min,max,appointstate);
        //用map存对象
        Map<String,Object> map = new HashMap<>();
        map.put("data",appointmentList);
        map.put("total",total);
        String json = JSON.toJSONString(map);
        return json;
    }


    //查询病人端用户状态
    @RequestMapping("/userState")
    @ResponseBody
    public String findUserState(HttpSession session){
        Tblmanager tblmanager = (Tblmanager) session.getAttribute("user");
        //查询病人端用户状态
        List<Tblappointment> list = tblappointmentListService.findUserState(tblmanager.getManagerId());
        return JSON.toJSONString(list);
    }

    //修改预约列表状态
    @RequestMapping("/updateUserState")
    @ResponseBody
    public String updateUserState(long appointmentId,long appointstate){
        int a = tblappointmentListService.updateAppointstate(appointmentId,appointstate);
        return String.valueOf(a);
    }

    //预约详情
    @RequestMapping("/bookDetail")
    @ResponseBody
    public String bookDetail(long appointmentId){
        List<Tblappointment> list = tblappointmentListService.bookDetail(appointmentId);
        return JSON.toJSONString(list);
    }

    //诊断答复界面详情
    @RequestMapping("/answerDetail")
    @ResponseBody
    public String answerDetail(long appointmentId){
        List<Tblappointment> list = tblappointmentListService.answer(appointmentId);
        return JSON.toJSONString(list);
    }


    //咨询师诊断答复
    @RequestMapping("/answer")
    @ResponseBody
    public String answer(long appointmentId,String answer){
      int a = tblappointmentListService.updateUserAnswer(appointmentId,answer);
        return String.valueOf(a) ;
    }


    //预约管理 预约列表
    //预约管理 预约列表总条数（判断页码）
    @RequestMapping("/Lists")
    @ResponseBody
    public String findLists(String realName, String userName, String min, String max,int page,int size){
        //页码起始值为：（页码-1）*当页条数
        page=(page-1)*size;
        //查询预约列表
        List<Tblappointment> appointmentList = tblappointmentListService.findAppointmentLists(realName,userName,min,max,page, size);
        int total = tblappointmentListService.findAppointmentListCounts(realName,userName,min,max);
        //用map存对象
        Map<String,Object> map = new HashMap<>();
        map.put("data",appointmentList);
        map.put("total",total);
        String json = JSON.toJSONString(map);
        return json;
    }


}
