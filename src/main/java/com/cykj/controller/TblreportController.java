package com.cykj.controller;

import com.alibaba.fastjson.JSON;
import com.cykj.bean.Tblappointment;
import com.cykj.bean.Tblmanager;
import com.cykj.bean.Tblreport;
import com.cykj.bean.UserTb;
import com.cykj.service.TblreportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = "/report", produces = "text/html;charset=utf-8")
public class TblreportController {

    @Autowired
    private TblreportService tblreportService;


    //查询报告报告
    @RequestMapping("/findReport")
    @ResponseBody
    public String findReport(String min, String max,String minScore,String maxScore, int page,int size){
        //页码起始值为：（页码-1）*当页条数
        page=(page-1)*size;
        //查询预约列表
        List<Tblreport> tblreports = tblreportService.findReport( min,max, minScore, maxScore,page, size);
        int total = tblreportService.findReportCount(min,max, minScore, maxScore);
        //用map存对象
        Map<String,Object> map = new HashMap<>();
        map.put("data",tblreports);
        map.put("total",total);
        String json = JSON.toJSONString(map);
        return json;
    }

    //查询报告报告
    @RequestMapping("/findReportDetail")
    @ResponseBody
    public String insertReport(long reportId){
        List<Tblreport> list = tblreportService.findReportDetail(reportId);
        return JSON.toJSONString(list);
    }
}
