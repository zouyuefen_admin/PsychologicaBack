package com.cykj.controller;

import com.alibaba.fastjson.JSON;
import com.cykj.bean.Tblmanager;
import com.cykj.bean.UserTb;
import com.cykj.service.TblmanagerService;
import com.cykj.service.TbluserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value = "/user", produces = "text/html;charset=utf-8")
public class UserController {
    @Autowired
    private TbluserService tbluserService;
    @Autowired
    private TblmanagerService tblmanagerService;


    //评估答题开销
    //第一步查询用户余额
    @RequestMapping("/spend")
    @ResponseBody
    public String Spend(long userId) {
        //第一步查询用户余额
        List<UserTb> userTbList = tbluserService.Balance(userId);
        return JSON.toJSONString(userTbList);
    }


    //第二步 查tblmanager表
    @RequestMapping("/manager1")
    @ResponseBody
    public String manager1(HttpSession session) {
        Tblmanager manager = (Tblmanager) session.getAttribute("user");
        List<Tblmanager> list = tblmanagerService.findDoctorById(manager.getManagerId());
        return JSON.toJSONString(list);
    }


    //第三步修改user表 修改tblmanager表咨询师余额
    @RequestMapping("/balance1")
    @ResponseBody
    public String balance1(long userId,double balance,HttpSession session) {
        //（1）修改user用户余额
        int a = tbluserService.Spend(userId,balance);
        //（2）修改tblmanager表咨询师余额
        if (a>0){
            Tblmanager manager = (Tblmanager) session.getAttribute("user");
            tblmanagerService.updateManagerBalanceById(manager.getManagerId());
        }
        return String.valueOf(a);


    }



    //第四步 插入bill表记录
    @RequestMapping("/bill1")
    @ResponseBody
    public String bill1(long userId,HttpSession session,double amount) {
        Tblmanager manager = (Tblmanager) session.getAttribute("user");
        //修改bill表
        int a = tbluserService.assessmentRecord(userId,manager.getManagerId(),amount);
        return String.valueOf(a);
    }


}

