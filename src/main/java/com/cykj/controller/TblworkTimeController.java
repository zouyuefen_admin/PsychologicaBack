package com.cykj.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cykj.bean.Tblmanager;
import com.cykj.bean.Tblpower;
import com.cykj.bean.Tblworktime;
import com.cykj.service.TblworkTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/workTime", produces = "text/html;charset=utf-8")
public class TblworkTimeController {
    @Autowired
    private TblworkTimeService tblworkTimeService;

    @RequestMapping("/setWorkTime")
    @ResponseBody
    public String powerList(HttpSession session, @RequestParam("times[]") List<String> times, String date) {
        for (String s : times) {
            System.out.println(s);
        }
        Tblmanager tblmanager = (Tblmanager) session.getAttribute("user");
        int i = tblworkTimeService.insertWorkTime(tblmanager.getManagerId(), date, times);
        System.out.println("设置时间" + i + "条");
        return i + "";
    }

    @RequestMapping("/findWorkTime")
    @ResponseBody
    public String findWorkTime(HttpSession session) {
        Tblmanager tblmanager = (Tblmanager) session.getAttribute("user");
        List<Tblworktime> findWorkTime = tblworkTimeService.findWorkTime(tblmanager.getManagerId());
        return JSON.toJSONString(findWorkTime);
    }


    @RequestMapping("/deleteWorkTime")
    @ResponseBody
    public String deleteWorkTime(HttpSession session,String workData) {
        Tblmanager tblmanager = (Tblmanager) session.getAttribute("user");
        int i = tblworkTimeService.deleteWorkTime(tblmanager.getManagerId(),workData);
        System.out.println("删除时间" + i + "条");
        return i + "";
    }

}
