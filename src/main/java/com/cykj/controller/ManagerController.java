package com.cykj.controller;

import com.alibaba.fastjson.JSON;

import com.cykj.bean.Tblmanager;
import com.cykj.bean.UserTb;
import com.cykj.service.TblareaService;
import com.cykj.service.TblmanagerService;
import com.cykj.service.TblpowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/manager", produces = "text/html;charset=utf-8")
public class ManagerController {
    @Autowired
    private TblmanagerService tblmanagerService;
    @Autowired
    private TblareaService tblareaService;

    //登录
    @RequestMapping("/login2")
    @ResponseBody
    public String login(String managerAcc, String managerPwd, String code, HttpSession session) {

        String pcCode = String.valueOf(session.getAttribute("code"));
        int i;
        if (pcCode.equals(code)) {
            Tblmanager tblmanager = tblmanagerService.managerLogin(managerAcc,managerPwd);
            if (tblmanager!=null){
                i=1;
                session.setAttribute("user",tblmanager);
            }else {
                i=0;
            }
        } else {
            i = 2;
        }

        String json = JSON.toJSONString(i);
        return json;
    }


    //客户管理
    @RequestMapping("/userManager")
    @ResponseBody
    public String userManager(UserTb userTb,int page,int size, HttpSession session) {
        //页码起始值为：（页码-1）*当页条数
        page=(page-1)*size;
        //查询客户数据
        List<UserTb> userTbsList = tblmanagerService.finduserList(userTb.getUserName(), (int) userTb.getUserSex(), userTb.getUserAge(), userTb.getUserPhone(), userTb.getUserState(), page, size);
        //查询客户数据总条数
        int total = tblmanagerService.findUserCount(userTb);
        //用map存对象
        Map<String,Object> map = new HashMap<>();
        map.put("data",userTbsList);
        map.put("total",total);
        String json = JSON.toJSONString(map);
        return json;
    }

    //启动、禁用(用户状态)
    @RequestMapping("/UserStateMsg")
    @ResponseBody
    public String UserState(@RequestParam("userId") int userId){
        //数据更新启动、禁用(用户状态)
        int i = tblmanagerService.updataUserState(userId);
        String json =JSON.toJSONString(i);
        System.out.println(i+ "------------------------------------");
        return json;
    }



    //删除客户
    @RequestMapping("/deleteUser")
    @ResponseBody
    public String deleteUser(@RequestParam("userId") int userId){
        //数据库删除user信息语句
        int deleteUser = tblmanagerService.delUserMsg(userId);
        //删除条数
        int i =  Integer.parseInt(JSON.toJSONString(deleteUser));
        //发送删除信息 条数
        String json = JSON.toJSONString(i);
        return json;
    }


    //重置密码
    @RequestMapping("/reUserPwd")
    @ResponseBody
    public String reUserPwd(@RequestParam("userId") int userId){
        //数据库更新user信息语句
        int dataUserPwd = tblmanagerService.updataUserPwd(userId);
        //更新条数
        int i =  Integer.parseInt(JSON.toJSONString(dataUserPwd));
        //发送更新信息 条数
        String json = JSON.toJSONString(i);
        return json;

    }






    //后台用户管理
    @RequestMapping("/backUserManager")
    @ResponseBody
    public String backUserManager(Tblmanager tblmanager, int page, int size, HttpSession session) {
        //页码起始值为：（页码-1）*当页条数
        page=(page-1)*size;
        //查询后台用户数据
        List<Tblmanager> tblmanagerList = tblmanagerService.findManagerList(tblmanager.getManagerId(), tblmanager.getRealName(), tblmanager.getTitles(), tblmanager.getSchool(), tblmanager.getManagerState(), page, size);
        //查询后台用户数据总条数
        int total = tblmanagerService.findManagerCount(tblmanager);
        //用map存对象
        Map<String,Object> map = new HashMap<>();
        map.put("data",tblmanagerList);
        map.put("total",total);
        String json = JSON.toJSONString(map);
        return json;
    }

    //后台用户 启动、禁用(用户状态)
    @RequestMapping("/backUserStateMsg")
    @ResponseBody
    public String backUserState(@RequestParam("managerId") long managerId){
        //数据更新启动、禁用(用户状态)
        int i = tblmanagerService.updataManagerState(managerId);
        String json =JSON.toJSONString(i);
        System.out.println(i+ "------------------------------------");
        return json;
    }



    //删除后台客户(逻辑删除，数据库中还有记录)
    @RequestMapping("/backDeleteUser")
    @ResponseBody
    public String backDeleteUser(@RequestParam("managerId") long managerId){
        //数据库删除user信息语句
        int deleteUser = tblmanagerService.updataManagerMsg(managerId);
        //删除条数
        int i =  Integer.parseInt(JSON.toJSONString(deleteUser));
        //发送删除信息 条数
        String json = JSON.toJSONString(i);
        return json;
    }


    //后台重置密码
    @RequestMapping("/backReUserPwd")
    @ResponseBody
    public String reUserPwd(@RequestParam("managerId") long managerId){
        //数据库更新user信息语句
        int dataUserPwd = tblmanagerService.updataManagerPwd(managerId);
        //更新条数
        int i =  Integer.parseInt(JSON.toJSONString(dataUserPwd));
        //发送更新信息 条数
        String json = JSON.toJSONString(i);
        return json;

    }

    //添加后台用户
    @RequestMapping("/insertManager")
    @ResponseBody
    public String insertManager(Tblmanager tblmanager,String managerAcc,String areaName){

        System.out.println("判断该用户是否已经存在："+tblmanagerService.findManager(tblmanager.getManagerAcc())+"条");
        int i;
        //判断该用户是否已经存在
        if (tblmanagerService.findManager(managerAcc)==0){
            //发送添加信息 条数
            i =  tblmanagerService.insertManager(tblmanager);
//            //在领域表里面做添加
//            int a=tblareaService.insertManagerArea(tblmanager.getManagerId(),areaName);
            System.out.println("添加"+i+"条数据");
        }else {
            //添加用户
           i=0;
            System.out.println("添加"+i+"条数据");
        }

        String json = JSON.toJSONString(i);
        return json;
    }


    //查状态
    @RequestMapping("/findDoctorStateByAcc")
    @ResponseBody
    public String findDoctorStateByAcc(String managerAcc){
        List<Tblmanager> list = tblmanagerService.findDoctorStateByAcc(managerAcc);
        return JSON.toJSONString(list);

    }

}
