package com.cykj.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

public class CodeUtils {
    public static String create(final int width, final int height, final String imgType, OutputStream output){
       //存储正确的验证码
       StringBuffer sb=new StringBuffer();
        Random random = new Random();

        //创建图片
        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
       //获取画笔
        Graphics graphics = image.getGraphics();
        //背景颜色
        graphics.setColor(new Color(247, 254, 72));
        graphics.fillRect(0,0,width,height);
        //颜色限制
        Color[] colors =new Color[]{Color.pink,Color.blue,Color.green,Color.BLACK,Color.CYAN,Color.RED};
        //在画板上生成干扰线（50 是线条个数）
        for (int i = 0; i <50 ; i++) {
            graphics.setColor(colors[random.nextInt(colors.length)]);
            final int x = random.nextInt(width);
            final int y = random.nextInt(height);
            final int w = random.nextInt(20);
            final int h = random.nextInt(20);
            final int signA = random.nextBoolean()?1:-1;
            final int signB = random.nextBoolean()?1:-1;
            graphics.drawLine(x,y,x+w*signA,y+h*signB);
        }
        //在花瓣上绘制字母
        graphics.setFont(new Font("Comic Sans MS",Font.BOLD,30));
        for (int i = 0; i <4 ; i++) {
            final int temp = random.nextInt(26)+97;
            String s= String.valueOf((char)temp);
            sb.append(s);
            graphics.setColor(colors[random.nextInt(colors.length)]);
            graphics.drawString(s,i*(width/4),height-(height/3));
        }
        graphics.dispose();
        try {
            ImageIO.write(image,imgType,output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  sb.toString();
    }
}
